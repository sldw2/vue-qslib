import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';

export class ParagraphSelOption extends BaseOption {

}
export default class ParagraphSubject extends BaseQuestionSubject<ParagraphSelOption> {
    public static option: ParagraphSelOption;
   
}
